/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.samointerface.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SAMOOutage implements Serializable {

    private String id; //klären unique ID
    private String voltageLevel;
    private String pressureLevel;
    private String description;
    private Date failureBegin;
    private Date failureEndResupplied;

}
