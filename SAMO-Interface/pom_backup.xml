<?xml version="1.0" encoding="UTF-8"?>
<!--
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.2.4.RELEASE</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>org.eclipse.openk</groupId>
	<artifactId>grid-failure-information.backend.saris-interface</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>grid-failure-information.saris-interface</name>
	<description>Grid-Failure-Information Project for openKONSEQUENZ</description>

	<properties>
		<skip.asciidoc>false</skip.asciidoc>
		<maven.test.skip>false</maven.test.skip>

		<java.version>21</java.version>
		<spring-cloud.version>Hoxton.RELEASE</spring-cloud.version>
		<springfox.version>2.9.2</springfox.version>
		<spring-security-test.version>5.2.1.RELEASE</spring-security-test.version>
		<powerMockReflect.version>2.0.0</powerMockReflect.version>
		<sonar-maven-plugin.version>3.2</sonar-maven-plugin.version>
		<asciidoctor-maven-plugin-version>1.5.3</asciidoctor-maven-plugin-version>
		<asciidoctorj-pdf-version>1.5.0-alpha.11</asciidoctorj-pdf-version>
		<asciidoctorj-version>1.5.4</asciidoctorj-version>
		<asciidoctorj-diagram-versions>1.5.4.1</asciidoctorj-diagram-versions>
		<jacoco-maven-plugin.version>0.8.12</jacoco-maven-plugin.version>
		<jruby-complete-version>9.0.0.0</jruby-complete-version>
		<mapstruct.version>1.2.0.Final</mapstruct.version>
		<flyway-core.version>6.0.8</flyway-core.version>
		<postgresql.version>42.2.8</postgresql.version>
		<lombock.version>1.18.10</lombock.version>
		<h2.version>1.4.200</h2.version>
		<jsonwebtoken.version>0.9.1</jsonwebtoken.version>
		<openfeign.version>2.2.0.RELEASE</openfeign.version>
		<keycloak-core.version>3.4.2.Final</keycloak-core.version>
		<nexmo.version>5.2.1</nexmo.version>
		<rabbitmq.version>5.2.0</rabbitmq.version>
	</properties>

	<dependencies>
		<!-- https://mvnrepository.com/artifact/org.springframework.amqp/spring-rabbit-test -->
		<dependency>
			<groupId>org.springframework.amqp</groupId>
			<artifactId>spring-rabbit-test</artifactId>
			<version>2.2.7.RELEASE</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-amqp</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-integration</artifactId>
			<version>2.2.5.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${lombok.version}</version>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.mapstruct</groupId>
			<artifactId>mapstruct-processor</artifactId>
			<version>${mapstruct.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${springfox.version}</version>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${springfox.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<groupId>org.junit.vintage</groupId>
					<artifactId>junit-vintage-engine</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.powermock</groupId>
			<artifactId>powermock-reflect</artifactId>
			<version>${powerMockReflect.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.batch</groupId>
			<artifactId>spring-batch-core</artifactId>
			<version>4.2.1.RELEASE</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.batch</groupId>
			<artifactId>spring-batch-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>com.rabbitmq</groupId>
			<artifactId>amqp-client</artifactId>
			<version>${rabbitmq.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-openfeign</artifactId>
			<version>${openfeign.version}</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
			<version>${openfeign.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-contract-wiremock</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-stream-test-support</artifactId>
			<scope>test</scope>
		</dependency>


	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<finalName>${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${jacoco-maven-plugin.version}</version>
				<configuration>
					<skip>${maven.test.skip}</skip>
					<output>file</output>
					<append>true</append>
					<excludes>
						<exclude>**/Globals.*</exclude>
					</excludes>
				</configuration>
				<executions>
					<execution>
						<id>jacoco-initialize</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>jacoco-site</id>
						<phase>verify</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.sonarsource.scanner.maven</groupId>
				<artifactId>sonar-maven-plugin</artifactId>
				<version>${sonar-maven-plugin.version}</version>
			</plugin>

		</plugins>
	</build>

</project>
