/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.exceptions.BadRequestException;
import org.eclipse.openk.gridfailureinformation.service.DistributionGroupMemberService;
import org.eclipse.openk.gridfailureinformation.viewmodel.DistributionGroupMemberDto;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("/distribution-groups")
public class DistributionGroupMemberController {
    private final DistributionGroupMemberService distributionGroupMemberService;

    public DistributionGroupMemberController(DistributionGroupMemberService distributionGroupMemberService) {
        this.distributionGroupMemberService = distributionGroupMemberService;
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen aller Verteilergruppenmitglieder")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @GetMapping("/members")
    public List<DistributionGroupMemberDto> findDistributionGroupMembers() {
        return distributionGroupMemberService.getDistributionGroupMembers();
    }

    @GetMapping("{groupUuid}/members/{uuid}")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen eines bestimmten Verteilergruppenmitgliedes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Mitglied wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public DistributionGroupMemberDto findMemberByUuid(
            @PathVariable UUID groupUuid,
            @PathVariable UUID uuid) {
        return distributionGroupMemberService.getMemberByUuid(groupUuid, uuid);
    }


    @GetMapping("{groupUuid}/members/csv")
    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen eines bestimmten Verteilergruppenmitgliedes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt"),
            @ApiResponse(responseCode = "404", description = "Mitglied wurde nicht gefunden")})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Resource> downloadMembersByUuidAsCSV(
            @PathVariable UUID groupUuid) {
        return distributionGroupMemberService.handleLoadFile(groupUuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-READER", "ROLE_GRID-FAILURE-CREATOR", "ROLE_GRID-FAILURE-QUALIFIER", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anzeigen aller Mitglieder einer bestimmten Verteilergruppe")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Erfolgreich durchgeführt")})
    @GetMapping("/{groupUuid}/members")
    public List<DistributionGroupMemberDto> findMembersByGroupId( @PathVariable UUID groupUuid) {
        return distributionGroupMemberService.getMembersByGroupId(groupUuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Anlegen eines neuen Mitglieds einer Verteilergruppe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Mitglied erfolgreich angelegt"),
            @ApiResponse(responseCode = "500", description = "Konnte nicht durchgeführt werden")
    })
    @PostMapping("/{groupUuid}/members")
    public ResponseEntity<DistributionGroupMemberDto> insertDistributionGroupMember(
            @PathVariable UUID groupUuid,
            @Validated @RequestBody DistributionGroupMemberDto memberDto) {
        if (!groupUuid.equals(memberDto.getDistributionGroupUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        DistributionGroupMemberDto savedDistributionGroupMemberDto = distributionGroupMemberService.insertDistributionGroupMember(groupUuid, memberDto);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .path("/{uuid}")
                .buildAndExpand(savedDistributionGroupMemberDto.getUuid())
                .toUri();
        return ResponseEntity.created(location).body(savedDistributionGroupMemberDto);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @ResponseStatus(HttpStatus.OK)
    @Operation(summary = "Ein bestimmtes Mitglied einer bestimmten Verteilergruppe löschen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Erfolgreich gelöscht"),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage"),
            @ApiResponse(responseCode = "404", description = "Nicht gefunden")})
    @DeleteMapping("/{groupUuid}/members/{memberUuid}")
    public void deleteCommunication(
            @PathVariable UUID groupUuid,
            @PathVariable UUID memberUuid) {
        distributionGroupMemberService.deleteDistributionGroupMember(groupUuid, memberUuid);
    }

    @Secured({"ROLE_GRID-FAILURE-ADMIN", "ROLE_GRID-FAILURE-PUBLISHER"})
    @Operation(summary = "Ein bestimmtes Mitglied einer bestimmten Verteilergruppe bearbeiten.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "404", description = "Verteilergruppenmitglied nicht gefunden."),
            @ApiResponse(responseCode = "400", description = "Ungültige Anfrage."),
            @ApiResponse(responseCode = "200", description = "Verteilergruppenmitglied erfolgreich geändert.")})
    @PutMapping("/{groupUuid}/members/{memberUuid}")
    public ResponseEntity<Resource> updateDistributionGroupMember(
            @PathVariable UUID groupUuid,
            @PathVariable UUID memberUuid,
            @Validated @RequestBody DistributionGroupMemberDto distributionGroupMemberDto) {
        if (!memberUuid.equals(distributionGroupMemberDto.getUuid())) {
            throw new BadRequestException("invalid.uuid.path.object");
        }
        distributionGroupMemberService.updateGroupMember(groupUuid, distributionGroupMemberDto);
        return ResponseEntity.ok().build();
    }

}
