echo ------- Login Keycloak -------
call kcadm config credentials --server http://localhost:8080/auth --realm master --user admin --password admin
REM ***************** CREATING NEW USER *****************
SET usernameVar=berta_qp
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Berta -s lastName=QP  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-publisher -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=rudi_qe
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Rudi -s lastName=QE  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-creator -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=lupin_qpe
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Lupin -s lastName=QPE  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-publisher --rolename grid-failure-creator -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=alexa_pe
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Alexa -s lastName=PE  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-creator --rolename grid-failure-publisher -r Elogbook
echo roles set
REM ***************** CREATING NEW USER *****************
SET usernameVar=delia_qpea
echo ------- Creating User: %usernameVar% -------
call kcadm create users -s username=%usernameVar% -s firstName=Delia -s lastName=QPEA  -s enabled=true -r Elogbook
call kcadm set-password -r Elogbook --username %usernameVar% --new-password %usernameVar%
echo pwd set
call kcadm add-roles --uusername %usernameVar% --rolename grid-failure-access --rolename grid-failure-qualifier --rolename grid-failure-publisher --rolename grid-failure-creator --rolename grid-failure-admin -r Elogbook
echo roles set

echo ------- Finished -------
